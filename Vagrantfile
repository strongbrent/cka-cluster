Vagrant.configure("2") do |config|
  config.vm.provider :virtualbox do |v|
    v.memory = 4096
    v.cpus = 2
  end

  config.vm.define :control do |control|
    control.vm.box = "bento/ubuntu-20.04"
    control.vm.hostname = "control"
    control.vm.network :private_network, ip: "10.0.0.10"

    control.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "ansible/control.yaml"
      ansible.extra_vars = {
        node_ip: "10.0.0.10"
      }
    end
  end

  %w{worker1 worker2}.each_with_index do |name, i|
    config.vm.define name do |worker|
      worker.vm.box = "bento/ubuntu-20.04"
      worker.vm.hostname = name
      worker.vm.network :private_network, ip: "10.0.0.#{i + 11}"

      worker.vm.provision "ansible_local" do |ansible|
        ansible.playbook = "ansible/worker.yaml"
        ansible.extra_vars = {
          node_ip: "10.0.0.#{i + 11}"
        }
      end
    end
  end

  config.vm.provision "shell" do |s|
    s.privileged = false
    s.inline     = "/bin/bash /vagrant/scripts/install_ansible.sh"
    s.binary     = true
  end

  config.vm.provision "ansible_local" do |ansible|
    ansible.playbook = "ansible/common.yaml"
  end
end
