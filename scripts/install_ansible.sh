#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
sudo apt-get update -y && sudo apt-get upgrade -y

if ! hash ansible >/dev/null 2>&1; then
  echo "Installing Ansible..."
  sudo apt-get update
  sudo apt-get install software-properties-common ansible git python-apt -y
else
  echo "Ansible already installed"
fi

