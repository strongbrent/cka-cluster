# CKA K8s Cluster
Quick and dirty, 3-node kubernetes cluster to use for practicing CKA certification tasks.

## Quick Start
Clone the repo  
Run `vagrant up`  
Wait a long time...  
Connect to the control node:  
```
vagrant ssh control
```  
Or connect to either of the two worker nodes:  
```
vagrant ssh worker1
```  

## Quick End
You can either halt the VMs:  
```
vagrant halt
```  
Or you can delete all of the VMs:  
```
vagrant destroy
```   

## K8s Cluster Information
The `Vagrantfile` deploys and provisions a 3-node kubernetes cluster. Most of the installation is automated using both `ansible` and `kubeadm`.  
  
**Warning**: I have a lot of RAM and 8 cores on my workstation, so the default memory and CPU settings are pretty hefty. Please adjust these settings before running `vagrant up`.  

### Common
**OS**: Ubuntu 20.04
**Docker CE**: latest  
**kubeadm**: latest  
**kubelet**: latest  
**kubectl**: latest  

**TODO**: eventually pin versions in `ansible`.

### 1 Control Node
- 2 CPUs  
- 4GB RAM  
- 62GB `/` partition  
- Private IP: 10.0.0.10 
- CNI Plugin: calico  
  - POD CIDR: 192.168.0.0/16 

### 2 Worker Nodes
- 2 CPUs each  
- 4GB RAM each  
- 62GB `/` partition each  
- Private IPs:  
  - 10.0.0.11  
  - 10.0.0.12  

### Cluster Details

cluster-info  
```
kubectl cluster-info
[output]
Kubernetes master is running at https://10.0.0.10:6443
KubeDNS is running at https://10.0.0.10:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
```
get nodes  
```
kubectl get nodes -o wide
[output]
NAME      STATUS   ROLES    AGE   VERSION   INTERNAL-IP   EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION     CONTAINER-RUNTIME
control   Ready    master   44m   v1.19.3   10.0.0.10     <none>        Ubuntu 20.04.1 LTS   5.4.0-42-generic   docker://19.3.13
worker1   Ready    <none>   31m   v1.19.3   10.0.0.11     <none>        Ubuntu 20.04.1 LTS   5.4.0-42-generic   docker://19.3.13
worker2   Ready    <none>   19m   v1.19.3   10.0.0.12     <none>        Ubuntu 20.04.1 LTS   5.4.0-42-generic   docker://19.3.13
```
get pods --all-namespaces  
```
kubectl get pods --all-namespaces
NAMESPACE     NAME                                      READY   STATUS    RESTARTS   AGE
kube-system   calico-kube-controllers-8f59968d4-l9p7x   1/1     Running   1          40m
kube-system   calico-node-4ncx2                         1/1     Running   1          15m
kube-system   calico-node-dk5wr                         1/1     Running   1          40m
kube-system   calico-node-w9247                         1/1     Running   1          28m
kube-system   coredns-f9fd979d6-bd24b                   1/1     Running   1          40m
kube-system   coredns-f9fd979d6-rz5l6                   1/1     Running   1          40m
kube-system   etcd-control                              1/1     Running   1          40m
kube-system   kube-apiserver-control                    1/1     Running   1          40m
kube-system   kube-controller-manager-control           1/1     Running   1          40m
kube-system   kube-proxy-jlnnw                          1/1     Running   1          40m
kube-system   kube-proxy-vnkjq                          1/1     Running   1          15m
kube-system   kube-proxy-xzrgb                          1/1     Running   1          28m
kube-system   kube-scheduler-control                    1/1     Running   1          40m
```